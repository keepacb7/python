#! /usr/bin/python3
import multiprocessing
import socket
import os
import subprocess
import time

HOST = '101.35.125.146'
#PORT=2181
def ip_check(x):
    PORT = x
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.settimeout(2)
    result = s.connect_ex((HOST,PORT))
#    result = "apple"

    ticks = time.asctime( time.localtime(time.time()) )
    print("time:%s: %s is %s",ticks,PORT,result)

if __name__ == '__main__':
    cores = 1000   #core可以计算也可以是自填，发挥最大cpu使用率即可
    pool = multiprocessing.Pool(processes=1000)
    tasks = range(1,60000)
    print(pool.map(ip_check,tasks))

